# FROM debian:buster
FROM buildpack-deps:buster

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ENV SERVICE_URL=https://marketplace.visualstudio.com/vscode
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 SHELL=/bin/bash
ENV LANGUAGE=${LANG} TZ=Asia/Seoul
ENV PATH=/usr/local/coder:/usr/local/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget curl bzip2 libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion \
	bash-completion \
	ca-certificates \
    software-properties-common \
    apt-transport-https \
    tzdata vim nano neovim git-lfs \
    openssl locales dumb-init \
    supervisor \
    cron \
    gpg \
	dirmngr 

 # timezone settings
RUN cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo $TZ > /etc/timezone

# Create a non-root user to use if preferred - see https://aka.ms/vscode-remote/containers/non-root-user.
# https://github.com/cdr/code-server/releases
# https://github.com/cdr/code-server/releases/download/2.1698/code-server2.1698-vsc1.41.1-linux-x86_64.tar.gz
# https://github.com/cdr/code-server/releases/download/3.0.0/code-server-3.0.0-linux-x86_64.tar.gz
# ENV vscode_version=2.1698
# ENV vscode_filename=code-server2.1698-vsc1.41.1-linux-x86_64
ENV vscode_version=3.0.0
ENV vscode_filename=code-server-3.0.0-linux-x86_64
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support for non-root user
    && apt-get install -y sudo \
    nodejs \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && mkdir -p /usr/local/coder \
    && wget https://github.com/cdr/code-server/releases/download/${vscode_version}/${vscode_filename}.tar.gz \
    && tar -xvf ${vscode_filename}.tar.gz \
    && mv ${vscode_filename} coder \
    && mv coder /usr/local/ \
    && chmod +x /usr/local/coder/code-server \
    && ln -s /usr/local/coder/code-server /usr/local/bin/code-server \
    && rm -f ${vscode_filename}.tar.gz \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN unset vscode_version vscode_filename
COPY ETC/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh

ENV vscode_icons_team_vscode_icons=vscode-icons-team.vscode-icons-9.7.0.vsix
ENV Atlassian_atlascode=Atlassian.atlascode-2.4.2.vsix
ENV redhat_vscode_yaml=redhat.vscode-yaml-0.7.2.vsix
ENV mhutchie_git_graph=mhutchie.git-graph-1.21.0.vsix
ENV formulahendry_code_runner=formulahendry.code-runner-0.9.15.vsix

COPY ETC/$vscode_icons_team_vscode_icons \
    ETC/$Atlassian_atlascode \
    ETC/$redhat_vscode_yaml \
    ETC/$mhutchie_git_graph \
    ETC/$formulahendry_code_runner \
    /home/vscode/
RUN code-server --install-extension /home/vscode/${vscode_icons_team_vscode_icons} \
    && code-server --install-extension /home/vscode/${Atlassian_atlascode} \
    && code-server --install-extension /home/vscode/${redhat_vscode_yaml} \
    && code-server --install-extension /home/vscode/${mhutchie_git_graph} \
    && code-server --install-extension /home/vscode/${formulahendry_code_runner}
USER vscode
RUN code-server --install-extension /home/vscode/${vscode_icons_team_vscode_icons} \
    && code-server --install-extension /home/vscode/${Atlassian_atlascode} \
    && code-server --install-extension /home/vscode/${redhat_vscode_yaml} \
    && code-server --install-extension /home/vscode/${mhutchie_git_graph} \
    && code-server --install-extension /home/vscode/${formulahendry_code_runner}
USER root
RUN unset vscode_icons_team_vscode_icons Atlassian_atlascode redhat_vscode_yaml mhutchie_git_graph formulahendry_code_runner

ENV ms_python_python=ms-python.python-2020.2.63072.vsix
ENV CoenraadS_bracket_pair_colorizer=CoenraadS.bracket-pair-colorizer-2-0.0.29.vsix
ENV jithurjacob_nbpreviewer=jithurjacob.nbpreviewer-1.2.2.vsix
ENV donjayamanne_python_extension_pack=donjayamanne.python-extension-pack-1.6.0.vsix
ENV tht13_python=tht13.python-0.2.3.vsix

COPY ETC/$ms_python_python \
    ETC/$CoenraadS_bracket_pair_colorizer \
    ETC/$jithurjacob_nbpreviewer \
    /home/vscode/

RUN code-server --install-extension /home/vscode/${ms_python_python} && \
    code-server --install-extension /home/vscode/${CoenraadS_bracket_pair_colorizer} && \
    code-server --install-extension /home/vscode/${jithurjacob_nbpreviewer} 

USER vscode

RUN code-server --install-extension /home/vscode/${ms_python_python} && \
    code-server --install-extension /home/vscode/${CoenraadS_bracket_pair_colorizer} && \
    code-server --install-extension /home/vscode/${jithurjacob_nbpreviewer} 
USER root

RUN unset ms_python_python CoenraadS_bracket_pair_colorizer jithurjacob_nbpreviewer donjayamanne_python_extension_pack tht13_python

ENV vscjava_vscode_java_pack=vscjava.vscode-java-pack-0.8.1.vsix 
ENV redhat_java=redhat.java-0.55.1.vsix
ENV vscjava_vscode_java_debug=vscjava.vscode-java-debug-0.24.0.vsix
ENV vscjava_vscode_java_dependency=vscjava.vscode-java-dependency-0.8.0.vsix
ENV vscjava_vscode_java_test=vscjava.vscode-java-test-0.22.1.vsix
ENV vscjava_vscode_maven=vscjava.vscode-maven-0.21.0.vsix
ENV adashen_vscode_tomcat=adashen.vscode-tomcat-0.11.1.vsix
ENV Pivotal_vscode_boot_dev_pack=Pivotal.vscode-boot-dev-pack-0.0.8.vsix
ENV Pivotal_vscode_spring_boot=Pivotal.vscode-spring-boot-1.15.0.vsix
ENV vscjava_vscode_spring_boot_dashboard=vscjava.vscode-spring-boot-dashboard-0.1.8.vsix
ENV vscjava_vscode_spring_initializr=vscjava.vscode-spring-initializr-0.4.6.vsix

COPY ETC/*-java-*.vsix \
  ETC/*.java-*.vsix \
  ETC/$vscjava_vscode_maven \
  ETC/$adashen_vscode_tomcat \
  ETC/$Pivotal_vscode_boot_dev_pack \
  ETC/*-spring-*.vsix \
  /home/vscode/

RUN code-server --install-extension /home/vscode/${vscjava_vscode_java_pack} \
    && code-server --install-extension /home/vscode/${redhat_java} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_debug} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_dependency} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_test} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_maven} \
    && code-server --install-extension /home/vscode/${adashen_vscode_tomcat} \
    && code-server --install-extension /home/vscode/${Pivotal_vscode_boot_dev_pack} \
    && code-server --install-extension /home/vscode/${Pivotal_vscode_spring_boot}\
    && code-server --install-extension /home/vscode/${vscjava_vscode_spring_boot_dashboard} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_spring_initializr}
USER vscode
RUN code-server --install-extension /home/vscode/${vscjava_vscode_java_pack} \
    && code-server --install-extension /home/vscode/${redhat_java} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_debug} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_dependency} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_java_test} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_maven} \
    && code-server --install-extension /home/vscode/${adashen_vscode_tomcat} \
    && code-server --install-extension /home/vscode/${Pivotal_vscode_boot_dev_pack} \
    && code-server --install-extension /home/vscode/${Pivotal_vscode_spring_boot}\
    && code-server --install-extension /home/vscode/${vscjava_vscode_spring_boot_dashboard} \
    && code-server --install-extension /home/vscode/${vscjava_vscode_spring_initializr}
USER root

RUN unset vscjava_vscode_java_pack redhat_java vscjava_vscode_java_debug vscjava_vscode_java_dependency \
    vscjava_vscode_java_test vscjava_vscode_maven adashen_vscode_tomcat Pivotal_vscode_boot_dev_pack \
    Pivotal_vscode_spring_boot vscjava_vscode_spring_boot_dashboard vscjava_vscode_spring_initializr

ENV rust_lang_rust=rust-lang.rust-0.7.0.vsix

COPY ETC/$rust_lang_rust \
    /home/vscode/
RUN code-server --install-extension /home/vscode/${rust_lang_rust}
USER vscode
RUN code-server --install-extension /home/vscode/${rust_lang_rust}
USER root

RUN rm -f /home/vscode/*.vsix

RUN locale-gen --purge
RUN locale-gen ko_KR.UTF-8
# RUN dpkg-reconfigure locales
# RUN echo 'LANG="ko_KR.UTF-8"' >> /etc/environment && \
#     # echo 'LANG="ko_KR.EUC-KR"' >> /etc/environment && \
#     echo 'LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/environment && \
#     echo 'LC_ALL="ko_KR.UTF-8"' >> /etc/environment
# RUN echo 'export LANG="ko_KR.UTF-8"' >> /etc/profile && \
#     # echo 'export LANG="ko_KR.EUC-KR"' >> /etc/profile && \
#     echo 'export LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/profile && \
#     echo 'export LC_ALL="ko_KR.UTF-8"' >> /etc/profile && \
#     echo "export QT_XKB_CONFIG_ROOT=/usr/share/X11/locale" >> /etc/profile
# RUN echo 'LANG="ko_KR.UTF-8"' >> /etc/default/locale && \
#     # echo 'LANG="ko_KR.EUC-KR"' >> /etc/default/locale && \
#     echo 'LANGUAGE="ko_KR;ko;en_GB;en"' >> /etc/default/locale && \
#     echo 'LC_ALL="ko_KR.UTF-8"' >> /etc/default/locale

# ENV PASSWORD "000000"
EXPOSE 6006-6015 8080 8888 8889
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
VOLUME [ "/home/vscode" ]
CMD [ "/bin/bash" ]