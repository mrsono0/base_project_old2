FROM mrsono0/base_project:miniconda3 as miniconda3
# https://github.com/rocker-org/rocker/blob/df56b98e4a2a4611fa9aacae99c4a304531c2640/r-base/Dockerfile
RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		ed \
		less \
		locales \
		vim-tiny \
		wget \
		ca-certificates \
		fonts-texgyre 
FROM miniconda3 as oraclejdk8
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
	gzip unzip zip \
	; \
	apt-get clean; \
	rm -rf /var/lib/apt/lists/*

COPY ETC/jdk-8u241-linux-x64.tar.gz /usr/local/
RUN cd /usr/local; tar -zxvf jdk-8u241-linux-x64.tar.gz; rm /usr/local/jdk-8u241-linux-x64.tar.gz ; cd /

ENV JAVA_HOME /usr/local/jdk1.8.0_241
ENV PATH $JAVA_HOME/bin:$PATH
RUN echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
  && echo "export PATH=${JAVA_HOME}/bin:$PATH" >> /etc/profile

RUN pip install beakerx pandas py4j \
    && beakerx install 
    # && cd /usr/local/share/jupyter/kernels; rm -r clojure groovy scala sql

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
    procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    && if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi \
    #
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
	
RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v9.0.33/bin/apache-tomcat-9.0.33.tar.gz \
    && tar xzf apache-tomcat-9.0.33.tar.gz; rm -rf apache-tomcat-9.0.33.tar.gz \
    && mv apache-tomcat-9.0.33 /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.61
RUN cd /usr/lib && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

FROM oraclejdk8

## Use Debian unstable via pinning -- new style via APT::Default-Release
RUN echo "deb http://http.debian.net/debian sid main" > /etc/apt/sources.list.d/debian-unstable.list \
        && echo 'APT::Default-Release "testing";' > /etc/apt/apt.conf.d/default

ENV R_BASE_VERSION 3.6.3

## Now install R and littler, and create a link for littler in /usr/local/bin
RUN apt-get update \
	&& apt-get install -t unstable -y --no-install-recommends \
		littler \
        r-cran-littler \
		r-base=${R_BASE_VERSION}-* \
		r-base-dev=${R_BASE_VERSION}-* \
		r-recommended=${R_BASE_VERSION}-* \
	&& ln -s /usr/lib/R/site-library/littler/examples/install.r /usr/local/bin/install.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/install2.r /usr/local/bin/install2.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/installGithub.r /usr/local/bin/installGithub.r \
	&& ln -s /usr/lib/R/site-library/littler/examples/testInstalled.r /usr/local/bin/testInstalled.r \
	&& install.r docopt \
	&& rm -rf /tmp/downloaded_packages/ /tmp/*.rds \
	&& rm -rf /var/lib/apt/lists/*

RUN echo "install.packages(c('repr', 'IRdisplay', 'RSentiment', 'IRkernel', 'igraph'), repos='https://mirror.las.iastate.edu/CRAN')" | R --vanilla
RUN echo "IRkernel::installspec(user = FALSE) | R --vanilla"
# RUN echo "setRepositories(ind=c(2)); source('https://bioconductor.org/biocLite.R'); biocLite('gRain',suppressUpdates=TRUE,ask=FALSE);biocLite('Rgraphviz',suppressUpdates=TRUE,ask=FALSE);" | R --vanilla
# Install R modules
RUN R -e "install.packages('rjson')" && \
	R -e "install.packages('XML')" && \
	R -e "install.packages('xml2')" && \
	R -e "install.packages('charlatan')" && \
	R -e "install.packages('httpuv')" && \
	R -e "install.packages('curl')" && \
	R -e "install.packages('httr')" && \
	R -e "install.packages('shiny')" && \
	R -e "install.packages('rmarkdown')" && \
	R -e "install.packages('knitr')" && \
	R -e "install.packages('caTools')" && \
	R -e "install.packages('writexl')" && \
	R -e "install.packages('rlist')" && \
	R -e "install.packages('tictoc')" && \
	R -e "install.packages('Rcpp')" && \
	R -e "install.packages('dplyr')" && \
	R -e "install.packages('ggplot2')" && \
	R -e "install.packages('kableExtra')" && \
	R -e "install.packages('tidyverse')" && \
	R -e "install.packages('remotes')" && \
	R -e "install.packages('devtools')" 
# Install R packages
RUN R CMD javareconf && R -e "install.packages('rJava')" && \
	R -e "install.packages('odbc')" && \
	R -e "install.packages('RJDBC')"

# RUN echo "PATH=$PATH" >> ~/.bashrc
RUN conda install -c r r-base r-irkernel && \
	conda install -c conda-forge r-rcpp r-devtools r-remotes 

COPY ETC/Mikhail-Arkhipov.r-0.0.6.vsix \
	 ETC/Ikuyadeu.r-1.1.8.vsix \
	 /home/vscode/
RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
USER vscode
RUN code-server --install-extension /home/vscode/Mikhail-Arkhipov.r-0.0.6.vsix \
	&& code-server --install-extension /home/vscode/Ikuyadeu.r-1.1.8.vsix
USER root
RUN rm -f /home/vscode/*.vsix