FROM mrsono0/base_project:python3.7

# https://github.com/docker-library/openjdk/blob/master/13/jdk/Dockerfile
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		bzip2 \
		unzip \
		xz-utils \
		ca-certificates p11-kit \
		binutils \
		fontconfig libfreetype6; 

ENV JAVA_HOME /usr/java/openjdk-13
ENV PATH $JAVA_HOME/bin:$PATH
RUN echo "export JAVA_HOME=${JAVA_HOME}" >> /etc/profile \
  && echo "export PATH=${JAVA_HOME}/bin:$PATH" >> /etc/profile

# backwards compatibility shim
RUN { echo '#/bin/sh'; echo 'echo "$JAVA_HOME"'; } > /usr/local/bin/docker-java-home && chmod +x /usr/local/bin/docker-java-home && [ "$JAVA_HOME" = "$(docker-java-home)" ]

# https://jdk.java.net/
# > Java Development Kit builds, from Oracle
ENV JAVA_VERSION 13.0.1
ENV JAVA_URL https://download.java.net/java/GA/jdk13.0.1/cec27d702aa74d5a8630c65ae61e4305/9/GPL/openjdk-13.0.1_linux-x64_bin.tar.gz
ENV JAVA_SHA256 2e01716546395694d3fad54c9b36d1cd46c5894c06f72d156772efbcf4b41335

RUN set -eux; \
	\
	wget -O openjdk.tgz "$JAVA_URL"; \
	echo "$JAVA_SHA256 */openjdk.tgz" | sha256sum -c -; \
	\
	mkdir -p "$JAVA_HOME"; \
	tar --extract \
		--file openjdk.tgz \
		--directory "$JAVA_HOME" \
		--strip-components 1 \
		--no-same-owner \
	; \
	rm openjdk.tgz; \
	\
	{ \
		echo '#!/usr/bin/env bash'; \
		echo 'set -Eeuo pipefail'; \
		echo 'if ! [ -d "$JAVA_HOME" ]; then echo >&2 "error: missing JAVA_HOME environment variable"; exit 1; fi'; \
# 8-jdk uses "$JAVA_HOME/jre/lib/security/cacerts" and 8-jre and 11+ uses "$JAVA_HOME/lib/security/cacerts" directly (no "jre" directory)
		echo 'cacertsFile=; for f in "$JAVA_HOME/lib/security/cacerts" "$JAVA_HOME/jre/lib/security/cacerts"; do if [ -e "$f" ]; then cacertsFile="$f"; break; fi; done'; \
		echo 'if [ -z "$cacertsFile" ] || ! [ -f "$cacertsFile" ]; then echo >&2 "error: failed to find cacerts file in $JAVA_HOME"; exit 1; fi'; \
		echo 'trust extract --overwrite --format=java-cacerts --filter=ca-anchors --purpose=server-auth "$cacertsFile"'; \
	} > /etc/ca-certificates/update.d/docker-openjdk; \
	chmod +x /etc/ca-certificates/update.d/docker-openjdk; \
	/etc/ca-certificates/update.d/docker-openjdk; \
	\
# https://github.com/docker-library/openjdk/issues/331#issuecomment-498834472
	find "$JAVA_HOME/lib" -name '*.so' -exec dirname '{}' ';' | sort -u > /etc/ld.so.conf.d/docker-openjdk.conf; \
	ldconfig; \
	java -Xshare:dump; \
	\
# basic smoke test
	javac --version; \
	java --version

RUN if [ ! -d "/usr/local/default-jdk" ]; then ln -s "${JAVA_HOME}" /usr/local/default-jdk; fi
## https://github.com/SpencerPark/IJava
RUN wget https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip
RUN mv ijava-1.3.0.zip /usr/local/; cd /usr/local; unzip ijava-1.3.0.zip; python /usr/local/install.py --sys-prefix; rm -r java install.py ijava-1.3.0.zip

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/root"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN apt-get update && \
    apt-get install -y \
      curl procps \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

RUN apt-get update \
    && apt-get -y install --no-install-recommends apt-utils dialog 2>&1 \
    #
    # Install git, process tools, lsb-release (common in install instructions for CLIs)
    && apt-get -y install procps lsb-release \
    #
    # Allow for a consistant java home location for settings - image is changing over time
    && if [ ! -d "/docker-java-home" ]; then ln -s "${JAVA_HOME}" /docker-java-home; fi \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN wget http://apache.mirror.cdnetworks.com/tomcat/tomcat-9/v9.0.31/bin/apache-tomcat-9.0.31.tar.gz \
	&& tar xzf apache-tomcat-9.0.31.tar.gz; rm -rf apache-tomcat-9.0.31.tar.gz \
	&& mv apache-tomcat-9.0.31 /usr/local/tomcat9; chmod -R 775 /usr/local/tomcat9/; chown -R root:vscode /usr/local/tomcat9
ENV CATALINA_HOME /usr/local/tomcat9
ENV PATH ${CATALINA_HOME}/bin:$PATH
ENV TOMCAT_NATIVE_LIBDIR ${CATALINA_HOME}/native-jni-lib
ENV LD_LIBRARY_PATH ${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}${TOMCAT_NATIVE_LIBDIR}

ENV KOTLIN_HOME /usr/lib/kotlinc
ENV PATH $PATH:$KOTLIN_HOME/bin
RUN echo "export KOTLIN_HOME=${KOTLIN_HOME}" >> /etc/profile
RUN echo "export PATH=${KOTLIN_HOME}/bin:$PATH" >> /etc/profile
ENV KOTLIN_VERSION=1.3.61
RUN cd /usr/lib && \
    wget https://github.com/JetBrains/kotlin/releases/download/v$KOTLIN_VERSION/kotlin-compiler-$KOTLIN_VERSION.zip && \
    unzip kotlin-compiler-*.zip && \
    rm kotlin-compiler-*.zip && \
    rm -f kotlinc/bin/*.bat

RUN unset JAVA_VERSION JAVA_BASE_URL JAVA_URL_VERSION

# COPY ETC/vscjava.vscode-java-pack-0.8.1.vsix \
# 	 ETC/redhat.java-0.53.1.vsix \
# 	 ETC/vscjava.vscode-java-debug-0.23.0.vsix \
# 	 ETC/vscjava.vscode-java-dependency-0.6.0.vsix \
# 	 ETC/vscjava.vscode-java-test-0.21.0.vsix \
# 	 ETC/vscjava.vscode-maven-0.20.0.vsix \
# 	 ETC/adashen.vscode-tomcat-0.11.1.vsix \

# 	 ETC/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
# 	 ETC/Pivotal.vscode-spring-boot-1.13.0.vsix \
# 	 ETC/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
# 	 ETC/vscjava.vscode-spring-initializr-0.4.6.vsix \
# 	 /home/vscode/

# RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
# 	&& code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \

# 	&& code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \

# 	&& code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
# 	&& code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix
# USER vscode
# RUN code-server --install-extension /home/vscode/vscjava.vscode-java-pack-0.8.1.vsix \
# 	&& code-server --install-extension /home/vscode/redhat.java-0.53.1.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-debug-0.23.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-dependency-0.6.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-java-test-0.21.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-maven-0.20.0.vsix \

# 	&& code-server --install-extension /home/vscode/adashen.vscode-tomcat-0.11.1.vsix \

# 	&& code-server --install-extension /home/vscode/Pivotal.vscode-boot-dev-pack-0.0.8.vsix \
# 	&& code-server --install-extension /home/vscode/Pivotal.vscode-spring-boot-1.13.0.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-spring-boot-dashboard-0.1.6.vsix \
# 	&& code-server --install-extension /home/vscode/vscjava.vscode-spring-initializr-0.4.6.vsix 
# USER root
# RUN rm -f /home/vscode/*.vsix