FROM mrsono0/devremotecontainers:oraclejdk8

ENV DEBIAN_FRONTEND noninteractive
RUN sed -i "s/archive.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/security.ubuntu.com/mirror.kakao.com/g" /etc/apt/sources.list \
    && sed -i "s/# deb-src/deb-src/g" /etc/apt/sources.list
RUN apt-get -y update --fix-missing \
    && apt-get -yy upgrade

RUN apt-get install -y alien libaio1 unixodbc gawk
ADD ETC/chkconfig /sbin/
RUN chmod 755 /sbin/chkconfig
RUN ln -s /usr/bin/gawk /bin/awk
RUN curl -o oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
# RUN alien --scripts -d oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm
RUN yum -y localinstall oracle-database*18c*

RUN mkdir /var/lock/subsys && \
    touch /var/lock/subsys/listener

# Clean up
RUN rm -rf oracle-database-preinstall-18c-1.0-1.el7.x86_64.rpm \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*